import pandas as pd
import numpy as np
dat = pd.read_csv('./19971123.csv')

dat.date=pd.to_datetime(dat[dat.columns[0]])
dat['year'] = dat.date.apply(lambda x: x.year)
dat['day'] = dat.date.apply(lambda x: x.day)
dat['month'] = dat.date.apply(lambda x: x.month)

h1 = u'Avg Wind Speed @ 20m [m/s]'
h2 = u'Avg Wind Speed @ 50m [m/s]'

shears = []
inshears = []
extps = []
higher_resolution_extps = []
year_exps = []
year_exts = []
for year in np.unique(dat.year):
    year_exps.append(np.nanmean(np.log(dat[h2]/dat[h1]) / np.log(50./20)))
    year_exts.append(np.nanmean(dat[h2] * (80./50)**year_exps[-1]))
    for day in np.unique(dat.day):
        subdat=dat[dat.year==year][dat.day==day]
        print subdat
        inse = []
        inext = []
        for month in np.unique(subdat.month):
            subdat2 = subdat[subdat.month==month]    
            se = np.nanmean(np.log(subdat2[h2]/subdat2[h1]) / np.log(50./20))
            inse.append(se)
            inext.append(subdat[h2] * (80./50)**se)
        se = np.nanmean(np.log(subdat[h2]/subdat[h1]) / np.log(50./20))
        shears.append(se)
        extps.append(np.nanmean(subdat[h2] * (80./50)**se))
        inshears.append(np.nanmean(inse))
        higher_resolution_extps.append(np.nanmean(inext))



print 'mean day-year shear exponent: S1 ',np.nanmean(shears)
print 'mean day-month-year shear exponent: S2 ',np.nanmean(inshears)
print 'mean annual shear exponent: S3 ', np.nanmean(year_exps)
print 'mean shear exponent: S0 ',np.nanmean(np.log(dat[h2]/dat[h1]) / np.log(50./20))

print 'mean extrapolated speed using S1 ',np.nanmean(extps)
print 'mean extrapolated speed using S2 ',np.nanmean(higher_resolution_extps)
print 'mean extrapolated speed using S3 ',np.nanmean(year_exts)
print 'mean extrapolated speed using mean shear exponent: S0 ',np.nanmean(dat[h2] * (80./50)**np.nanmean(np.log(dat[h2]/dat[h1]) / np.log(50./20)))
